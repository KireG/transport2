<?php

namespace TR\Models;
use TR\Models\Basicmodel;

require_once 'Basicmodel.php';


class Trip extends Basicmodel {

    public function query(){  
        return $sql= "SELECT trips.id,trips.date,trips.reference,trips.route, trips.additional_info,trips.number_of_shipments,trips.kilometers,trips.tons,b.name AS begin_country,e.name  AS end_country,drivers.name AS driver, customers.name AS customer, vehicles.registration
        FROM trips JOIN countries as b ON trips.begin_country=b.id JOIN countries as e ON trips.end_country=e.id JOIN drivers ON drivers.id=trips.driver_id JOIN customers on customers.id=trips.customer_id JOIN vehicles ON vehicles.id=trips.vehicle_id            
        ORDER BY trips.id DESC";    
    } 

}