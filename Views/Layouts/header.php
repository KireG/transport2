<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script type="text/javascript" src="javascript.js"></script>
    <link href="../Publics/styles.css" rel="stylesheet">

</head>
<body>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 header">
            <div class="row">
                <div class="col md-3 titlediv"><h3>Transport Manager</h3></div>
                <div class="col md-3 offset-md-6 buttdiv">
                    <a href=""><button type="btn" class=" btn lbuttons">Login</button></a>
                    <a href=""><button type="btn" class=" btn lbuttons">Register</button></a>
                    <a href=""><button type="btn" class=" btn lbuttons">Logout</button></a>
                </div>
            </div>    
        </div>     
    </div>
    
    