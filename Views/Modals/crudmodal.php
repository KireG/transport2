<!-- DELETE MODAL -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header hed1">
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body bod1">Do you want to delete this trip?</div>
        <div class="modal-footer foot1">
         <button type="button" class="btn deltripbtn" data-bs-dismiss="modal">Delete</button> 
        </div>
      </div>
    </div>
  </div>
  <!--Add modal -->
  <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div id="errordiv" class="alert alert-danger" >Please insert all data</div>
          <form name="trip" id="addnewtrip" method="post">
            <div class="mb-3">
              <label for="date" class="form-label">Date</label>
              <input type="date" class="form-control" id="date" name="date" aria-describedby="dateHelp">
            </div>
            <div class="mb-3">
              <label for="reference" class="form-label">Reference</label>
              <input type="text" class="form-control" id="reference" name="reference" aria-describedby="referenceHelp">
            </div>
            <div class="mb-3">
              <label for="customer" class="form-label">Customer</label>
              <select type="text" class="form-control" id="customer" name="customer" aria-describedby="customerHelp">
               <option value="null"></option>
                <?php foreach($trips as $trip){?>
                   <option value="<?php $trip->id ?>"><?php $trip->customer->name ?></option>
                <?php }?>
              </select>
            </div>
            <!-- <div class="mb-3">
              <label for="begin_country" class="form-label">Begin country</label>
              <select type="text" class="form-control" id="begin_country" name="begin_country" aria-describedby="begin_countryHelp">
               <option value="null"></option>
                @foreach($countries as $country)
                   <option value="{{$country->id}}">{{$country->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="mb-3">
              <label for="end_country" class="form-label">End country</label>
              <select type="text" class="form-control" id="end_country" name="end_country" aria-describedby="end_countryHelp">
                <option value="null"></option>
                @foreach($countries as $country)
                   <option value="{{$country->id}}">{{$country->name}}</option>
                @endforeach
              </select>
              </select>
            </div>
            <div class="mb-3">
              <label for="route" class="form-label">Route</label>
              <input type="text" class="form-control" id="route" name="route" aria-describedby="routeHelp">
            </div>
            <div class="mb-3">
              <label for="vehicle" class="form-label">Vehicle</label>
              <select type="text" class="form-control" id="vehicle" name="vehicle" aria-describedby="vehicleHelp">
                <option value="null"></option>
                @foreach($vehicles as $vehicle)
                   <option value="{{$vehicle->id}}">{{$vehicle->registration}}</option>
                @endforeach
              </select>  
            </div>
            <div class="mb-3">
              <label for="additional_info" class="form-label">Additional info</label>
              <input type="text" class="form-control" id="additional_info" name="additional_info" aria-describedby="additional_infoHelp">
            </div>
            <div class="mb-3">
              <label for="driver" class="form-label">Driver</label>
              <select type="text" class="form-control" id="driver" name="driver" aria-describedby="driverHelp">
                <option value="null"></option>
                @foreach($drivers as $driver)
                   <option value="{{$driver->id}}">{{$driver->name}}</option>
                @endforeach
              </select>  
            </div> -->
            <div class="mb-3 cc">
              <label for="number_of_shipments" class="form-label">Number of shipments</label>
              <input type="text" class="form-control" name="number_of_shipments" id="number_of_shipments" aria-describedby="number_of_shipmentsHelp">
            </div>
            <div class="mb-3 cc">
              <label for="kilometers" class="form-label">Kilometers</label>
              <input type="text" class="form-control" name="kilometers" id="kilometers" aria-describedby="kilometersHelp">
            </div>
            <div class="mb-3 cc">
              <label for="tons" class="form-label">Tons</label>
              <input type="text" class="form-control" name="tons" id="tons" aria-describedby="tonsHelp">
            </div>
          
          <div class="modal-footer"> 
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>  
          </form>


        </div>

      </div>
    </div>
  </div>