<?php

use TR\Models\Trip;
//use KG\Models\Abstracts\Basicmodel;


require_once '../Models/Trip.php';

$trips=(new Trip)->getobject();
// $trips->getobject();

?>
<?php
include 'Layouts/header.php';
?>
<div class="row mainrow">
    <div class="col-md-10 offset-md-1 main">
        <div class="row">
            <div class="col-md-1" id="adddiv">
            <button class="btn" id="addbtn" data-bs-toggle="modal" data-bs-target="#addModal">Add Trip</button>
            </div> 
        </div>
        <div class="row">
            <div class="col-md-12">
            <table class="tables table table-striped" id="tab1">
                <thead id="kir">
                  <tr>
                      <th>No.</th>
                      <th>Date of Loading</th>
                      <th>Reference</th>
                      <th>Customer</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Maps</th>
                      <th>Truck</th>
                      <th>Tralier</th>
                      <th>Driver</th>
                      <th>Shipments</th>
                      <th>Km</th>
                      <th>Tons</th>
                      <th></th>
                      <th></th>
                  </tr>
                </thead> 
                <tbody id="tab">
                <?php foreach($trips as $trip){
                    echo "<tr>
                    <th>".$trip['id']."</th>
                    <th>".$trip['date']."</th>
                    <th>".$trip['reference']."</th>
                    <th>".$trip['customer']."</th>
                    <th>".$trip['begin_country']."</th>
                    <th>".$trip['end_country']."</th>
                    <th>".$trip['route']."</th>
                    <th>".$trip['registration']."</th>
                    <th>".$trip['driver']."</th>
                    <th>".$trip['id']."</th>
                    <th>".$trip['number_of_shipments']."</th>
                    <th>".$trip['kilometers']."</th>
                    <th>".$trip['tons']."</th>
                    <th><a class='edlink'>Edit</a></th>
                    <th><a class='dellink' data-bs-toggle='modal' data-bs-target='#exampleModal'>Delete</a></th>  
                    </tr>";
                }  ?>
                </tbody>
            </table>   
            </div>
        </div>
   
    </div>
</div>

<?php
include 'Modals/crudmodal.php';
include 'Layouts/footer.php';
?>